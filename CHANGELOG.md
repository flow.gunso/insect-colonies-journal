# Changelog
All notable changes to this project will be documented in this file.

**Types of changes**  
* Added for new features.
* Changed for changes in existing functionality.
* Deprecated for soon-to-be removed features.
* Removed for now removed features.
* Fixed for any bug fixes.
* Security in case of vulnerabilities.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## **1.1.0** | 2021/04/02
### Added
- IndividualCategory, Individual and IndividualCount objects were added to replace the Count object (#8 !3)
- Field colonies in the Event object (#4 !3)
### Changed
- _EventCategory_ field _name_ was renamed _label_ (!3)
### Deprecated
- Object Count (#8)
- Field colony in the Event object (#4 !3)
### Removed
- Count object from the admin pages (#8 !3)


## **1.0.1** | 2021/03/25
### Changed
- Configure for production (#1 !2)


## **1.0.0** | 2021/03/25
### Added
- Write the prototype (!1)
