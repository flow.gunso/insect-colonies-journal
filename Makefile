
flake8:
	flake8 insect_colonies_journal/ api/ manage.py

black:
	read -p "Warning: this command will modify your code! (Press any key to continue or Ctrl-C to exit)" continue
	black insect_colonies_journal/ api/ manage.py

makemigrations:
	python manage.py makemigrations api

migrate:
	python manage.py migrate api