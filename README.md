**Insect Colonies Journal**  
A web application to journalize and track event related to insect colonies.


# Supported tags
The application is available within the [project's container registry](https://gitlab.com/flow.gunso/insect-colonies-journal/container_registry/1817519).

`latest`, `1`, `1.1`, `1.1.0`: Insect Colonies Journal [v1.1.0](https://gitlab.com/flow.gunso/insect-colonies-journal/-/tags/1.1.0)  
`proxy-latest`, `proxy-1`, `proxy-1.0`, `proxy-1.0.0`: Insect Colonies Journal's proxy [v1.0.0](https://gitlab.com/flow.gunso/insect-colonies-journal/-/tags/1.0.0+proxy)
