from django.contrib import admin

from api.models import (
    Taxon,
    Colony,
    EventCategory,
    Event,
    Individual,
    IndividualCount,
    IndividualCategory,
)


@admin.register(Taxon)
class TaxonAdmin(admin.ModelAdmin):
    list_display = ["family", "subfamily", "tribe", "genus", "species", "subspecies"]
    list_filter = ["family", "subfamily", "tribe", "genus", "species", "subspecies"]


@admin.register(Colony)
class ColonyAdmin(admin.ModelAdmin):
    def hash_id(self, obj):
        return f"#{obj.id}"

    def count_events(self, obj):
        return obj.events.count()

    hash_id.short_description = "hash id"
    count_events.short_description = "events count"

    list_display = ["hash_id", "founded_on", "taxon", "count_events"]
    list_filter = ["taxon"]


@admin.register(EventCategory)
class EventCategoryAdmin(admin.ModelAdmin):
    def count_events(self, obj):
        return obj.events.count()

    count_events.short_description = "events count"

    list_display = ["label", "count_events"]


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    def get_colonies(self, obj):
        return ", ".join(str(colony) for colony in obj.colonies.all())

    get_colonies.short_description = "colonies"

    list_display = ["created_on", "get_colonies", "category", "notes"]
    list_filter = ["category", "colonies"]
    exclude = ["colony"]


@admin.register(Individual)
class IndividualAdmin(admin.ModelAdmin):
    list_display = ["label", "category"]
    list_filter = ["category"]


@admin.register(IndividualCategory)
class IndividualCategoryAdmin(admin.ModelAdmin):
    list_display = ["label"]


@admin.register(IndividualCount)
class IndividualCountAdmin(admin.ModelAdmin):
    def get_event_colonies(self, obj):
        return ", ".join(str(colony) for colony in obj.event.colonies.all())

    def get_event_created_on(self, obj):
        return obj.event.created_on

    get_event_colonies.short_description = "colony"
    get_event_created_on.short_description = "created on"

    list_display = ["get_event_colonies", "get_event_created_on", "individual", "count"]
    list_filter = ["event__colonies", "event__created_on", "individual"]
