from django.db import models
from django.utils import timezone
from django.core.exceptions import ValidationError


class Taxon(models.Model):
    family = models.CharField(max_length=255, blank=True)
    subfamily = models.CharField(max_length=255, blank=True)
    tribe = models.CharField(max_length=255, blank=True)
    genus = models.CharField(max_length=255, blank=True)
    species = models.CharField(max_length=255, blank=True)
    subspecies = models.CharField(max_length=255, blank=True)

    def __str__(self):
        string = f"{self.genus.capitalize()} {self.species}"
        if self.subspecies:
            string += f" {self.subspecies}"
        return string

    class Meta:
        verbose_name = "taxon"
        verbose_name_plural = "taxa"
        unique_together = [
            "family",
            "subfamily",
            "tribe",
            "genus",
            "species",
            "subspecies",
        ]


class Colony(models.Model):
    founded_on = models.DateTimeField()
    taxon = models.ForeignKey(
        "Taxon", related_name="colonies", on_delete=models.PROTECT
    )

    def __str__(self):
        return f"#{self.id} {self.taxon}"

    class Meta:
        verbose_name = "colony"
        verbose_name_plural = "colonies"


class EventCategory(models.Model):
    label = models.CharField(max_length=255)

    def __str__(self):
        return self.label.capitalize()

    class Meta:
        verbose_name = "event category"
        verbose_name_plural = "event categories"


class Event(models.Model):
    created_on = models.DateTimeField(default=timezone.now)
    colony = models.ForeignKey(
        "Colony",
        related_name="events_legacy",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        default=None,
    )
    colonies = models.ManyToManyField("Colony", related_name="events")
    category = models.ForeignKey(
        "EventCategory", related_name="events", on_delete=models.PROTECT
    )
    notes = models.TextField(blank=True)

    def clean(self):
        event_count_category = EventCategory.objects.get(label="count")
        if self.category == event_count_category and self.colonies.count() > 1:
            raise ValidationError(
                "Cannot assign more than one colony to a count event."
            )

    class Meta:
        verbose_name = "event"
        verbose_name_plural = "events"


class Count(Event):
    workers = models.IntegerField()
    brood = models.IntegerField()

    def save(self, *args, **kwargs):
        if not self.pk:
            count_category = EventCategory.objects.get(name="count")
            self.category = count_category
            super().save(*args, **kwargs)

    class Meta:
        verbose_name = "count"
        verbose_name_plural = "counts"


class Individual(models.Model):
    label = models.CharField(max_length=255)
    category = models.ForeignKey(
        "IndividualCategory",
        related_name="individuals",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        default=None,
    )

    def __str__(self):
        return self.label.capitalize()

    class Meta:
        verbose_name = "individual"
        verbose_name_plural = "individuals"


class IndividualCategory(models.Model):
    label = models.CharField(max_length=255)

    def __str__(self):
        return self.label.capitalize()

    class Meta:
        verbose_name = "individual category"
        verbose_name_plural = "individuals categories"


class IndividualCount(models.Model):
    event = models.ForeignKey(
        "Event", related_name="individuals_counts", on_delete=models.CASCADE
    )
    individual = models.ForeignKey(
        "Individual", related_name="+", on_delete=models.PROTECT
    )
    count = models.IntegerField()

    def clean(self):
        event_count_category = EventCategory.objects.get(label="count")
        if self.event.category != event_count_category:
            raise ValidationError(
                "Cannot assign individual count to events that are not count events."
            )

    class Meta:
        verbose_name = "individual count"
        verbose_name_plural = "individuals counts"
