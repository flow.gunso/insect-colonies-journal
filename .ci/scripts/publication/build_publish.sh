#!/bin/bash

SCRIPT=$(basename ${BASH_SOURCE[0]})

usage() {
    echo "Build and push the Docker images to Gitlab's registry."
    echo ""
    echo "$SCRIPT [options] build_target"
    echo
    echo "Build targets are:"
    echo "  application     To build the application."
    echo "  proxy           To build the proxy."
    echo "Only one target can be built at a time."
    echo
    echo "Options are:"
    echo "  -h, --help      Print this helper."

    if [ "$1" ]; then
        echo ""
        echo "Exited with error: $1"
        exit 1
    else
        exit 0
    fi
}
 
while (( "$#" )); do
    case "$1" in
        -h|--help)
            usage
            ;;
        -*|--*)
            usage "Unknown option $1"
            ;;
        *) # Break to reduce footprint.
            break
            ;;
    esac
done

# Validate positional arguments.
if [ $# -eq 0 ]; then
    usage
elif [ $# -gt 1 ]; then
    usage "Too many build target given."
fi

# Define the variable according to the build target.
case "$1" in
    application)
        semver_regex="^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$"
        tag_prefix=""
        docker_file=.ci/files/production/application/Dockerfile
        ;;
    proxy)
        semver_regex="^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+proxy){1}$"
        tag_prefix="proxy-"
        docker_file=.ci/files/production/proxy/Dockerfile
        ;;
    *)
        usage "Unknown build target $1"
        ;;
esac


# Validate the tag against the SemVer.
semver_match=$(echo $CI_COMMIT_TAG | grep -P $semver_regex)
if [ -z $semver_match ]; then
    echo "Version tag $CI_COMMIT_TAG does not adhere to SemVer!"
    exit 1
fi

# Capture version components and assign the tag list.
major=$(echo $CI_COMMIT_TAG | perl -pe "s/$semver_regex/\1/")
minor=$(echo $CI_COMMIT_TAG | perl -pe "s/$semver_regex/\2/")
revision=$(echo $CI_COMMIT_TAG | perl -pe "s/$semver_regex/\3/")
tags=("${tag_prefix}latest")
tags+=("${tag_prefix}$major")
tags+=("${tag_prefix}$major.$minor")
tags+=("${tag_prefix}$major.$minor.$revision")

# Build the proxy.
docker build -f $docker_file -t $CI_REGISTRY_IMAGE:build .

# Push the image tags.
docker login --username $CI_REGISTRY_DEPLOY_USERNAME --password $CI_REGISTRY_DEPLOY_PASSWORD $CI_REGISTRY
for tag in "${tags[@]}"; do
    docker tag $CI_REGISTRY_IMAGE:build $CI_REGISTRY_IMAGE:$tag
    docker push $CI_REGISTRY_IMAGE:$tag
done
