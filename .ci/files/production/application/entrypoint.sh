# Bash entrypoint:
# Make user database is reachable before starting server process

wait_for_database() {
    pg_isready database
}

python manage.py migrate
python manage.py collectstatic --no-input

gunicorn -b 0.0.0.0:8000 insect_colonies_journal.wsgi
