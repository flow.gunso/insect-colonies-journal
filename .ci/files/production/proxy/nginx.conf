# From https://docs.gunicorn.org/en/stable/deploy.html

worker_processes    auto;
user                nginx nginx;
error_log           stderr;

events {
    worker_connections    2048;
    accept_mutex          off;
    multi_accept          on;
    use                   epoll;
}

http {
    include       mime.types;
    default_type  application/octet-stream;
    access_log    stdout;

    upstream application_upstream {
        server application:8000 fail_timeout=0;
    }

    server {
        listen                  80 deferred;
        client_max_body_size    10M;
        keepalive_timeout       5;
        server_name             _;

        location /static/ {
            alias       /var/www/html/statics/;
        }
        location / {
            try_files   $uri @application;
        }

        location @application {
            proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
            proxy_set_header    X-Forwarded-Proto   $scheme;
            proxy_set_header    Host                $http_host;
            proxy_redirect      off;
            proxy_pass          http://application_upstream;
        }
    }
}