from insect_colonies_journal.settings import *  # noqa
from insect_colonies_journal.settings import BASE_DIR


ENV_DIR = BASE_DIR / ".env"

SECRET_KEY = "jn*%^0!imci0&a*ysm5&mo8ye&$t6@m$scro0!w721tl1o)(y#"

DEBUG = True

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": ENV_DIR / "db" / "db.sqlite3",
    }
}

STATIC_ROOT = ENV_DIR / "statics"
