from insect_colonies_journal.settings import *  # noqa
from insect_colonies_journal.settings import get_environment_variable


SECRET_KEY = get_environment_variable("APP_SECRET")

DEBUG = False

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": get_environment_variable("POSTGRES_DB"),
        "USER": get_environment_variable("POSTGRES_USER"),
        "PASSWORD": get_environment_variable("POSTGRES_PASSWORD"),
        "HOST": get_environment_variable("POSTGRES_HOST", "database"),
        "PORT": get_environment_variable("POSTGRES_PORT", "5432"),
    }
}

STATIC_ROOT = "/statics"
